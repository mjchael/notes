import { watch } from "vue";

export function useWatchCharacters(valueToWatch: any, maxLength: number) {
  watch(valueToWatch, (newValue, oldValue) => {
    // console.log("newValue", newValue);
    // console.log("oldValue", oldValue);

    if (newValue.length === maxLength) {
      alert(`Only ${maxLength} chararacter allowed`);
    }
  });
}
