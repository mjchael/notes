import { defineStore } from "pinia";

export const useStoreNotes = defineStore({
  id: "storeNotes",
  state: () => ({
    notes: [
      {
        id: 1,
        content: "This is my first note",
      },
      {
        id: 2,
        content: "This is a second note",
      },
    ],
  }),
  actions: {
    addNote(newNote: string) {
      this.notes.unshift({
        id: Math.round(Math.random() * 999999999),
        content: newNote,
      });
    },
    deleteNote(id: number) {
      this.notes = this.notes.filter((note) => note.id !== id);
    },
    updateNote(id: number, note: string) {
      const index = this.notes.findIndex((note) => (note.id = id));
      this.notes[index] = {
        id: id,
        content: note,
      };
    },
  },
  getters: {
    getNoteContent: (state) => {
      return (id: number) => {
        return state.notes.filter((note) => {
          return note.id === id;
        })[0].content;
      };
    },
    totalNotesCount: (state) => {
      return state.notes.length;
    },
    noteCharacterCount: (state) => {
      return state.notes.reduce((a, b) => a + b.content.length, 0);
    },
  },
});
